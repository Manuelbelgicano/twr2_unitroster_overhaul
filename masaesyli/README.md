# Masaesyli

**Total units:** 25

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 5
	- Slave Infantry
	- Numidian Light Infantry
	- Gaetuli Tribesmen
	- Desert Cohort
	- Desert Legionaries

- Spear Infantry: 4
	- Numidian Spearmen
	- Desert Vigiles
	- Armoured Numidian Spearmen
	- Numidian Noble Infantry

- Missile Infantry: 4
	- Tribesmen
	- Tribal Slingers
	- Numidian Javelinmen
	- Heavy Numidian Skirmishers

- Melee Cavalry: 3
	- Desert Cavalry
	- Armoured Desert Cavalry
	- Desert Legionary Cavalry

- Shock Cavalry: 2
	- Numidian Riders
	- Armoured Numidian Riders

- Missile Cavalry: 4
	- Gaetuli Horse Skirmishers
	- Numidian Cavalry
	- Armoured Numidian Cavalry
	- Numidian Noble Cavalry

- Elephant: 1
	- African Elephants

- Chariot: 2
	- Desert Chariots
	- Armoured Desert Chariots