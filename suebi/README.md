# Suebi

**Total units:** 22

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 7
	- Club Levy
	- Hex-Bearers
	- Bloodsworm
	- Round Shield Swordsmen
	- Wolf Warriors
	- Berserkers
	- Sword Masters

- Spear Infantry: 7
	- Germanic Tribesmen
	- Spear Levy
	- Spear Brothers
	- Spearwomen
	- Spear Wall
	- Night Hunters
	- Wodanaz Spears

- Missile Infantry: 5
	- Germanic Slingers
	- Germanic Youths
	- Horse Runners
	- Longbow Hunters
	- Cimbri Bow-Women

- Melee Cavalry: 2
	- Riders of the Hunt
	- Noble Riders

- Missile Cavalry: 1
	- Germanic Scout Riders