# Carthage

**Total units:** 13

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 2
	- Mob
	- Libyan Infantry

- Spear Infantry: 4 (6)
	- Citizen Militia
	- (Late) Libyan Hoplites
	- (Late) Carthaginian Hoplites
	- Sacred Band

- Pike Infantry: 1
	- African Pikemen

- Missile Infantry: 2
	- Libyan Javelinmen
	- Libyan Peltasts

- Melee Cavalry: 1
	- Carthaginian Cavalry

- Shock Cavalry: 1
	- Noble Cavalry

- Elephant: 2
	- African Elephants
	- African War Elephants