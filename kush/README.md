# Kush

**Total units:** 24

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 6
	- Slave Infantry
	- Kushite Slave Infantry
	- Swordsmen
	- Disciples of Apedemak
	- Shotel Warriors
	- Armoured Shotel Warriors

- Spear Infantry: 3
	- Kushite Slave Spearmen
	- Nubian Spearmen
	- Leopard Warriors

- Pike Infantry: 1
	- Kushite Pikes

- Missile Infantry: 6
	- Tribesmen
	- Slave Slingers
	- Archers
	- Nubian Bowmen
	- Kushite Archers
	- Royal Kushite Archers

- Melee Cavalry: 4
	- Desert Cavalry
	- Aethiopian Cavalry
	- Armoured Desert Cavalry
	- Kushite Royal Guard

- Elephant: 2
	- African Elephants
	- African War Elephants

- Chariot: 2
	- Desert Chariots
	- Scythed Chariots