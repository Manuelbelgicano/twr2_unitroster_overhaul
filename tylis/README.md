# Tylis

**Total units:** 13

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 5
	- Celtic Warriors
	- Thracian Warriors
	- Galo-Thracian Infantry
	- Tribal Warriors
	- Oathsworn

- Spear Infantry: 3
	- Celtic Tribesmen
	- Levy Freemen
	- Spear Warriors

- Missile Infantry: 2
	- Celtic Slingers
	- Celtic Skirmishers

- Melee Cavalry: 2
	- Light Horse
	- Noble Horse

- Missile Cavalry: 1
	- Raiding Horsemen