# Nabatea

**Total units:** 28

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 4
	- Desert Levy
	- Nabatean Swordsmen
	- Nabatean Axe Warriors
	- Noble Swordsmen

- Spear Infantry: 6
	- Levy Spearmen
	- Desert Hoplites
	- Caravan Guard
	- Armoured Desert Hoplites
	- Heavy Desert Spearmen
	- Rekem Palace Guard

- Pike Infantry: 2
	- Desert Pikemen
	- Nabatean Thorax Pikes

- Missile Infantry: 5
	- Tribes People
	- Slingers
	- Levy Skirmishers
	- Nabatean Light Peltasts
	- Nabatean Heavy Archers

- Melee Cavalry: 6
	- Camel Spearmen
	- Desert Cavalry
	- Arabian Cavalry
	- Armoured Desert Cavalry
	- Armoured Camel Spearmen
	- Nabatean Noble Cavalry

- Shock Cavalry: 2
	- Desert Heavy Lancers
	- Hellenic Desert Cataphracts

- Missile Cavalry: 1
	- Camel Archers

- Chariot: 2
	- Desert Chariots
	- Scythed Chariots