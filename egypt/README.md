# Egypt

**Total units:** 32

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 9
	- Mob
	- Egyptian Infantry
	- Karian Axemen
	- Galatian Swordsmen
	- Sobek Cultists
	- Thorax Swordsmen
	- Royal Thorax Swordsmen
	- Royal Peltasts
	- Galatian Royal Guard

- Spear Infantry: 3
	- Levy Thureos Spears
	- Thureos Spears
	- Nubian Spearmen

- Pike Infantry: 4
	- Egyptian Pikemen
	- Pikemen
	- Thorax Pikemen
	- Hellenic Royal Guard

- Missile Infantry: 5 (6)
	- Egyptian Javelinmen
	- Egyptian Slingers
	- Egyptian Archers
	- Nubian Archers
	- (Light) Peltasts

- Melee Cavalry: 4
	- Camel Spearmen
	- Light Cavalry
	- Citizen Cavalry
	- Egyptian Cavalry

- Shock Cavalry: 1
	- Ptolemaic Cavalry

- Missile Cavalry: 3
	- Skirmisher Cavalry
	- Camel Archers
	- Tarantine Cavalry

- Elephant: 2
	- African Elephants
	- African War Elephants

- Chariot: 1
	- Scythed Chariots