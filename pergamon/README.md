# Pergamon

**Total units:** 20

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 2
	- Mob
	- Galatian Swords

- Spear Infantry: 5
	- Militia Hoplites
	- Thureos Spears
	- Galatian Spears
	- Hoplites
	- Agema Spears

- Pike Infantry: 2
	- Levy Pikemen
	- Pikemen

- Missile Infantry: 5 (6)
	- Javelinmen
	- Slingers
	- Archers
	- (Light) Peltasts
	- Picked Peltasts

- Melee Cavalry: 2
	- Light Horse
	- Citizen Cavalry

- Shock Cavalry: 2
	- Hippeus Lancers
	- Pergamenes Noble Cavalry

- Missile Cavalry: 2
	- Skirmisher Cavalry
	- Tarantine Cavalry