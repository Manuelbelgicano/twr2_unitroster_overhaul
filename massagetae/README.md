# Massagetae

**Total units:** 14

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 1
	- Young Axes

- Spear Infantry: 2
	- Tribesmen
	- Steppe Spearmen

- Missile Infantry: 1
	- Steppe Archers

- Shock Cavalry: 5
	- Steppe Lancers
	- Steppe Armoured Lancers
	- Steppe Noble Lancers
	- Saka Noble Armoured Lancers
	- Saka Cataphracts

- Missile Cavalry: 5
	- Steppe Horse Skirmishers
	- Steppe Horse Archers
	- Armoured Horse Archers
	- Noble Horse Archers
	- Saka Cataphracts Horse Archers