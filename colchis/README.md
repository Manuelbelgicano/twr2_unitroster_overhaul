# Colchis

**Total units:** 16

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 4
	- Mob
	- Hillmen
	- Axemen
	- Kartli Axemen

- Spear Infantry: 3
	- Eastern Spearmen
	- Hoplites
	- Colchian Nobles

- Missile Infantry: 4
	- Eastern Javelinmen
	- Eastern Slingers
	- Eastern Archers
	- Peltasts

- Melee Cavalry: 2
	- Citizen Cavalry
	- Noble Blood Cavalry

- Shock Cavalry: 1
	- Hippeus Lancers

- Missile Cavalry: 2
	- Horse Skirmishers
	- Horse Archers