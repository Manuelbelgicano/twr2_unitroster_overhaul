# Armenia

**Total units:** 20

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 4
	- Mob
	- Hillmen
	- Axemen
	- Kartli Axemen

- Spear Infantry: 3
	- Eastern Spearmen
	- Persian Hoplites
	- Noble Spearmen

- Missile Infantry: 4
	- Eastern Javelinmen
	- Eastern Slingers
	- Eastern Archers
	- Elite Persian Archers

- Melee Cavalry: 2
	- Noble Blood Cavalry
	- Azat Knights

- Shock Cavalry: 3
	- Persian Cavalry
	- Eastern Cataphracts
	- Royal Cataphracts

- Missile Cavalry: 4
	- Horse Skirmishers
	- Horse Archers
	- Armoured Horse Archers
	- Noble Horse Archers