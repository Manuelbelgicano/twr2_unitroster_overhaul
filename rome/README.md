# Rome

**Total units:** 22 (without auxiliaries)

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 10 (16)
	- Plebs
	- Socii Hastati
	- Socii Extraordinarii
	- Gladiators
	- Gladiatrices
	- Hastati
	- Principes
	- Legionaries (Hastati, Principes)
	- Veteran Legionaries (Triarii)
	- First Cohort
	- Legionary Cohort (Legionaries)
	- Evocati Cohort (Veteran Legionaries)
	- Eagle Cohort (First Cohort)
	- Armoured Legionaries
	- Praetorians
	- Praetorian Guard (Praetorians)

- Spear Infantry: 5 (6)
	- Rorarii
	- Auxiliary Infantry
	- Vigiles (Rorarii)
	- Spear Gladiators
	- Spear Gladiatrices
	- Triarii

- Missile Infantry: 2
	- Leves
	- Velites

- Melee Cavalry: 4 (5) 
	- Equites
	- Socii Equites
	- Legionary Cavalry (Equites)
	- Auxiliary Cavalry
	- Praetorian Cavalry

- Shock Cavalry: 1
	- Socii Equites Extraordinarii