# Arevaci

**Total units:** 20

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 6
	- Iberian Swordsmen
	- ~~Guerrilla Warriors~~
	- **Painted Warriors**
	- *Celtiberian Warriors*
	- Scutarii
	- *Celtiberian Fighters*
	- Noble Fighters

- Spear Infantry: 4
	- Iberian Tribesmen
	- Iberian Spearmen
	- Scutarii Spearmen
	- *Celtiberian Guard*

- Missile Infantry: 4
	- Iberian Skirmishers
	- Iberian Slingers
	- Balearic Slingers
	- *Guerrilla Warriors*

- Melee Cavalry: 3
	- Iberian Cavalry
	- Celtiberian Cavalry
	- Noble Cavalry

- Shock Cavalry: 1
	- *Epona's Riders*

- Missile Cavalry: 2
	- Cantabrian Cavalry
	- *Noble Cantabrian Cavalry*
