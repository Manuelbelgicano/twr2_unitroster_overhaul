# Pontus

**Total units:** 19

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 3
	- Mob
	- Hillmen
	- Pontic Swordsmen

- Spear Infantry: 3
	- Eastern Spearmen
	- Thureos Spears
	- Hoplites

- Pike Infantry: 3
	- Levy Pikemen
	- Pikemen
	- Bronze Shield Pikemen

- Missile Infantry: 4
	- Eastern Javelinmen
	- Eastern Slingers
	- Eastern Archers
	- Pontic Peltasts

- Melee Cavalry: 3
	- Citizen Cavalry
	- Noble Blood Cavalry
	- Cappadocian Cavalry

- Shock Cavalry: 1
	- Pontic Royal Cavalry

- Missile Cavalry: 1
	- Horse Skirmishers

- Chariot: 1
	- Scythed Chariots