# Arverni

**Total units:** 16

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 4
	- Celtic Warriors
	- Naked Warriors
	- Chosen Swordsmen
	- Oathsworn

- Spear Infantry: 5
	- Celtic Tribesmen
	- Levy Freemen
	- Spear Warriors
	- Chosen Spearmen
	- Spear Nobles

- Missile Infantry: 4
	- Celtic Youths
	- Celtic Slingers
	- Celtic Skirmishers
	- Gallic Hunters

- Melee Cavalry: 3
	- Light Horse
	- Heavy Horse
	- Noble Horse