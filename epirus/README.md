# Epirus

**Total units:** 26

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 3
	- Mob
	- Illyrian Levies
	- Royal Peltasts

- Spear Infantry: 4
	- Citizen Hoplites
	- Milia Hoplites
	- Thureos Spears
	- Hoplites

- Pike Infantry: 3
	- Levy Pikemen
	- Pikemen
	- Hellenic Royal Guard

- Missile Infantry: 5
	- Javelinmen
	- Slingers
	- Archers
	- Agrianian Axemen
	- Peltasts

- Melee Cavalry: 2
	- Citizen Cavalry
	- Aspis Companion Cavalry

- Shock Cavalry: 2
	- Thessalian Cavalry
	- Hellenic Royal Cavalry

- Missile Cavalry: 2
	- Skirmisher Cavalry
	- Tarantine Cavalry