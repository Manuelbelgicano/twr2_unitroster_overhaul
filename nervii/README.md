# Nervii

**Total units:** 16

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 5
	- Celtic Warriors
	- Guerrilla Swordsmen
	- Naked Warriors
	- Fierce Swords
	- Oathsworn

- Spear Infantry: 4
	- Celtic Tribesmen
	- Levy Freemen
	- Spear Brothers
	- Naked Spears

- Missile Infantry: 4
	- Celtic Youths
	- Celtic Slingers
	- Celtic Skirmishers
	- Gallic Hunters

- Melee Cavalry: 3
	- Light Horse
	- Mighty Horse
	- Noble Horse