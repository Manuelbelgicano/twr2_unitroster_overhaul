# Cimmeria

**Total units:** 18

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 1
	- Young Axes

- Spear Infantry: 7
	- Tribesmen
	- Steppe Spearmen
	- Citizen Hoplites
	- Militia Hoplites
	- Scythian Hoplites
	- Hoplites
	- Cimmerian Noble Infantry

- Missile Infantry: 5
	- Javelinmen
	- Slingers
	- Steppe Archers
	- Cimmerian Heavy Archers
	- Picked Peltasts

- Melee Cavalry: 1
	- Citizen Cavalry

- Shock Cavalry: 1
	- Steppe Lancers

- Missile Cavalry: 3
	- Steppe Horse Archers
	- Armoured Horse Archers
	- Noble Horse Archers