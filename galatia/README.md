# Galatia

**Total units:** 12

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 3
	- Galatian Swords
	- Naked Swords
	- Galatian Legionaries

- Spear Infantry: 4
	- Celtic Tribesmen
	- Levy Freemen
	- Galatian Spears
	- Galatian Noblemen

- Missile Infantry: 2
	- Celtic Slingers
	- Celtic Skirmishers

- Melee Cavalry: 2
	- Light Horse
	- Noble Horse

- Missile Cavalry: 1
	- Galatian Raiders