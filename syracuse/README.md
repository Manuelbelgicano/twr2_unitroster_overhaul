# Syracuse

**Total units:** 19

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 2
	- Mob
	- Thorax Swordsmen

- Spear Infantry: 8
	- Citizen Hoplites
	- Militia Hoplites
	- Light Hoplites
	- Thureos Spears
	- Thureos Hoplites
	- Hoplites
	- Thorax Hoplites
	- Picked Hoplites

- Pike Infantry: 1
	- Pikemen

- Missile Infantry: 4 (5)
	- Javelinmen
	- Slingers
	- Archers
	- (Light) Peltasts

- Melee Cavalry: 1
	- Citizen Cavalry

- Shock Cavalry: 1
	- Hippeus Lancers

- Missile Cavalry: 2
	- Skirmisher Cavalry
	- Tarantine Cavalry