# Odrysian Kingdom

**Total units:** 20

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 6
	- *Thracian Tribal Infantry*
	- Thracian Warriors
	- *Helo-Thracian Warriors*
	- *Thracian Thureos Swordsmen*
	- Thracian Nobles
	- *Thracian Royal Peltasts*

- Spear Infantry: 4
	- Spears
	- *Thracian Spears*
	- *Thracian Thureos Spears*
	- *Thracian Hoplites*

- Missile Infantry: 5
	- Tribal Garrison
	- Thracian Slingers
	- **Thracian Bowmen**
	- Thracian Skirmishers
	- Thracian Peltasts

- Melee Cavalry: 3
	- Thracian Horsemen
	- *Armoured Thracian Horsemen*
	- Thracian Royal Cavalry

- Missile Cavalry: 2
	- Thracian Cavalry
	- *Armoured Thracian Cavalry*
