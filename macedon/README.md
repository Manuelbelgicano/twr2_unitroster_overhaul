# Macedon

**Total units:** 23

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 3
	- Mob
	- Thorax Swordsmen
	- Royal Peltasts

- Spear Infantry: 4
	- Militia Hoplites
	- Thureos Spears
	- Hoplites
	- Shield Bearers

- Pike Infantry: 4
	- Levy Pikemen
	- Pikemen
	- Thorax Pikemen
	- Foot Companions

- Missile Infantry: 5 (6)
	- Javelinmen
	- Slingers
	- Archers
	- (Light) Peltasts
	- Agrianian Axemen

- Melee Cavalry: 2
	- Citizen Cavalry
	- Aspis Companion Cavalry

- Shock Cavalry: 3
	- Sarissa Cavalry
	- Thessalian Cavalry
	- Companion Cavalry

- Missile Cavalry: 2
	- Skirmisher Cavalry
	- Tarantine Cavalry