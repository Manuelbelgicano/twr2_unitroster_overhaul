# Baktria

**Total units:** 26

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 3
	- Mob
	- Bactrian Hillmen
	- Thorax Swordsmen

- Spear Infantry: 4
	- Eastern Spearmen
	- Thureos Spears
	- Scale Thorax Hoplites
	- Bactrian Royal Guard

- Pike Infantry: 3
	- Levy Pikemen
	- Pikemen
	- Thorax Pikemen

- Missile Infantry: 6 (7)
	- Eastern Javelinmen
	- Eastern Spearmen
	- Persian Light Archers
	- (Light) Peltasts
	- Elite Persian Archers
	- Bactrian Peltasts

- Melee Cavalry: 3
	- Bactrian Light Horse
	- Citizen Cavalry
	- Bactrian Noble Horse

- Shock Cavalry: 2
	- Bactrian Royal Cavalry
	- Hellenic Cataphracts

- Missile Cavalry: 3
	- Horse Skirmishers
	- Horse Archers
	- Balearic Horse Archers

- Elephant: 2
	- Indian War Elephants
	- Indian Armoured Elephants