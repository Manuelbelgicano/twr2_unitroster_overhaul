# Royal Scythia

**Total units:** 15

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 1
	- Young Axes

- Spear Infantry: 2
	- Tribesmen
	- Steppe Spearmen

- Missile Infantry: 1
	- Steppe Archers

- Melee Cavalry: 1
	- Scythian Royal Horse

- Shock Cavalry: 2
	- Steppe Armoured Lancers
	- Steppe Noble Lancers

- Missile Cavalry: 8
	- Steppe Horse Skirmishers
	- Steppe Horse Archers
	- Scythian 'Amazonian' Riders
	- Armoured Horse Archers
	- Scythian Noblewomen
	- Noble Horse Archers
	- Royal Horse Archers
	- Scythian Royal Skirmishers