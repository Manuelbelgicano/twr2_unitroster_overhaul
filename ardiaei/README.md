# Ardiaei

**Total units:** 20

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 4
    - Illyrian Levies
    - **Illyrian Raiders**
    - *Illyrian Warriors*
    - *Illyrian Nobles*

- Spear Infantry: 6
    - Illyrian Tribesmen
    - Illyrian Spearmen
    - Illyrian Thureos Spears
    - Illyrian Marines
    - Illyrian Hoplites
    - Illyrian Noble Hoplites

- Missile Infantry: 4
    - Slave Javelinmen
    - Slave Slingers
    - *Slave Archers*
    - *Illyrian Heavy Skirmishers*

- Melee Cavalry: 2
    - Illyrian Cavalry
    - *Illyrian Armoured Cavalry*

- Shock Cavalry: 2
    - *Illyrian Sarissa Cavalry*
    - *Illyrian Armoured Lancers*

- Missile Cavalry: 2
    - *Illyrian Pillagers*
    - *Illyrian Marauders*