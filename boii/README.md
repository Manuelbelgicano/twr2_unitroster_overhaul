# Boii

**Total units:** 15

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 5
	- Celtic Warriors
	- Axe Warriors
	- Naked Warriors
	- Sword Followers
	- Oathsworn

- Spear Infantry: 4
	- Celtic Tribesmen
	- Levy Freemen
	- Spear Warriors
	- Veteran Spears

- Missile Infantry: 3
	- Celtic Slingers
	- Celtic Bowmen
	- Celtic Skirmishers

- Melee Cavalry: 3
	- Light Horse
	- Heavy Horse
	- Noble Horse