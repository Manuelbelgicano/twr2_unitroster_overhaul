# Getae

**Total units:** 15

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 2
	- Falxmen
	- Noble Swords

- Spear Infantry: 6
	- Dacian Tribesmen
	- Spears
	- Spear Warriors
	- Heavy Spears
	- Armoured Spears
	- Noble Spears

- Missile Infantry: 4
	- Dacian Bowmen
	- Dacian Skirmishers
	- Dacian Heavy Bowmen
	- Dacian Heavy Skirmishers

- Melee Cavalry: 1
	- Spear Horsemen

- Shock Cavalry: 1
	- Noble Horsemen

- Missile Cavalry: 1
	- Bow Horsemen