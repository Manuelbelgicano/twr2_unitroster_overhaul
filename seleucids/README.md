# Seleucids

**Total units:** 34

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 5
	- Mob
	- Hillmen
	- Thorax Swordsmen
	- Silver Shield Swordsmen
	- Royal Peltasts

- Spear Infantry: 4
	- Eastern Spearmen
	- Persian Hoplites
	- Thureos Spears
	- Shield Bearers

- Pike Infantry: 4
	- Levy Pikemen
	- Pikemen
	- Thorax Pikemen
	- Silver Shield Pikemen

- Missile Infantry: 7 (8)
	- Javelinmen
	- Eastern Javelinmen
	- Eastern Slingers
	- Archers
	- Persian Light Archers
	- (Light) Peltasts
	- Syrian Heavy Archers

- Melee Cavalry: 5
	- Camel Spearmen
	- Light Cavalry
	- Citizen Cavalry
	- Median Cavalry
	- Azat Knights

- Shock Cavalry: 2
	- Agema Cavalry
	- Hellenic Cataphracts

- Missile Cavalry: 4
	- Horse Skirmishers
	- Skirmisher Cavalry
	- Camel Archers
	- Tarantine Cavalry

- Elephant: 2
	- Indian War Elephants
	- Indian Armoured Elephants

- Chariot: 1
	- Scythed Chariots