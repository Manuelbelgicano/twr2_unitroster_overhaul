# Parthia

**Total units:** 22

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 3
	- Mob
	- Hillmen
	- Parthian Swordsmen

- Spear Infantry: 2
	- Eastern Spearmen
	- Persian Hoplites

- Missile Infantry: 5
	- Eastern Javelinmen
	- Eastern Slingers
	- Persian Light Archers
	- Parthian Foot Archers
	- Elite Persian Archers

- Melee Cavalry: 3
	- Camel Spearmen
	- Median Cavalry
	- Noble Blood Cavalry

- Shock Cavalry: 3
	- Eastern Cataphracts
	- Camel Cataphracts
	- Royal Cataphracts

- Missile Cavalry: 5
	- Horse Skirmishers
	- Camel Archers
	- Parthian Horse Archers
	- Armoured Horse Archers
	- Noble Horse Archers

- Elephant: 1
	- Indian War Elephants