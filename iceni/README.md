# Iceni

**Total units:** 16

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 6
	- Sword Band
	- Ambushers
	- Painted Ones
	- Druidic Nobles
	- Chosen Sword Band
	- Heroic Nobles

- Spear Infantry: 4
	- Farmers
	- Levy Freemen
	- Spear Band
	- Chosen Spear Band

- Missile Infantry: 2
	- Briton Skirmishers
	- Briton Slingers

- Melee Cavalry: 3
	- Briton Scout Riders
	- Veteran Riders
	- Heroic Riders

- Chariot: 1
	- Chariots