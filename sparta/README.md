# Sparta

**Total units:** 16

Changed units will be shown in **bold** and new units in *italics*.

- Spear Infantry: 6
	- Spartan Youths
	- Periokoi Spears
	- Periokoi Hoplites
	- Spartan Hoplites
	- Royal Spartans
	- Heroes of Sparta

- Pike Infantry: 2
	- Periokoi Pikemen
	- Spartan Pikemen

- Missile Infantry: 5
	- Helot Javelinmen
	- Helot Slingers
	- Helot Archers
	- Gorgo's Skirmishers
	- Periokoi Peltasts

- Melee Cavalry: 1
	- Citizen Cavalry

- Missile Cavalry: 2
	- Skirmisher Cavalry
	- Tarantine Cavalry