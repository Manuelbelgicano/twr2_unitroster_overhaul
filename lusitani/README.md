# Lusitani

**Total units:** 15

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 5
	- Iberian Swordsmen
	- Iberian Swordswomen
	- Guerrilla Warriors
	- Veteran Shield Warriors
	- Lusitani Nobles

- Spear Infantry: 4
	- Iberian Tribesmen
	- Lusitani Guerrillas
	- Scutarii Spearmen
	- Lusitani Spearmen

- Missile Infantry: 3
	- Iberian Skirmishers
	- Iberian Slingers
	- Balearic Slingers

- Melee Cavalry: 2
	- Iberian Cavalry
	- Scutarii Cavalry

- Missile Cavalry: 1
	- Cantabrian Cavalry