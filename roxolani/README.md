# Roxolani

**Total units:** 13

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 1
	- Young Axes

- Spear Infantry: 2
	- Tribesmen
	- Steppe Spearmen

- Missile Infantry: 1
	- Steppe Archers

- Melee Cavalry: 2
	- Sarmatian Riders
	- Sarmatian Horsemen

- Shock Cavalry: 4
	- Steppe Lancers
	- Steppe Armoured Lancers
	- Steppe Noble Lancers
	- Sarmatian Royal Lancers

- Missile Cavalry: 3
	- Steppe Horse Archers
	- Armoured Horse Archers
	- Noble Horse Archers