# Saba

**Total units:** 25

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 4
	- Desert Levy
	- Sabaean Swordsmen
	- Mercenary Maas Gat Marauders
	- Noble Swordsmen

- Spear Infantry: 5
	- Desert Spearmen
	- Sabaean Spearmen
	- Caravan Guard
	- Mercenary Ma'rib Guard
	- Ma'rib Royal Guard

- Missile Infantry: 4
	- Tribes People
	- Slingers
	- Levy Skirmishers
	- Sabaean Archers

- Melee Cavalry: 6
	- Camel Spearmen
	- Desert Cavalry
	- Arabian Cavalry
	- Mercenary Himyar Cavalry
	- Armoured Camel Spearmen
	- Royal Ma'rib Cavalry

- Shock Cavalry: 4
	- Camel Lancers
	- Desert Heavy Lancers
	- Sabaean Camel Cataphracts
	- Ma'rib Camel Cataphracts

- Missile Cavalry: 2
	- Camel Archers
	- Royal Camel Archers

- Chariot: 1
	- Desert Chariots