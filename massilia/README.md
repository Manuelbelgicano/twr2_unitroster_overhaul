# Massilia

**Total units:** 19

Changed units will be shown in **bold** and new units in *italics*.

- Melee Infantry: 3
	- Celtic Warriors
	- Axe Warriors
	- Thorax Swordsmen

- Spear Infantry: 7
	- Celtic Tribesmen
	- Citizen Hoplites
	- Levy Freemen
	- Light Hoplites
	- Massalian Thureos Spears
	- Hoplites
	- Massalian Hoplites

- Missile Infantry: 3
	- Celtic Slingers
	- Celtic Skirmishers
	- Peltasts

- Melee Cavalry: 3
	- Light Horse
	- Citizen Cavalry
	- Massalian Cavalry

- Shock Cavalry: 1
	- Hippeus Lancers

- Missile Cavalry: 2
	- Skirmisher Cavalry
	- Tarantine Cavalry